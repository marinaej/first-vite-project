import React from 'react';
import styled from 'styled-components';

function Button(){

    const handleClick = () => {
        window.location.reload();
  };
    
    const Button= styled.button`
    height: 50px;
    width: 150px;
    display: flex;
    border-radius: 10px;
    color: #753A1C;
    border: solid 2px #753A1C;
    justify-content: center;
    align-items: center;
    padding: 10px;
    `

    return(
        <Button onClick={handleClick}>
            <img src="https://img.icons8.com/ios-glyphs/30/000000/refresh--v1.png" alt="" />
            <p>Got more jokes?</p>
        </Button>
    )
}

export default Button;