import React, {Suspense} from 'react'
const Quote = React.lazy(() => import('./Quote'));
import styled from 'styled-components';

function Card() {
  const imgNumber = [1,2,3,4,5,6,7,8];
  const randomImage = imgNumber[Math.floor(Math.random()*imgNumber.length)];

  const imgSrc = `src/assets/image${randomImage}.jpeg`;

    const Card = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 20px;
    margin: 10px;
    background-color: white;
    color: #753A1C;
    border: solid 2px #753A1C;
    box-shadow: 10px 10px;
    width: 300px;
    min-width: 200px;
    height: 300px;
    border-radius: 30px;
    font-size: 16px;
    `;

  return (
    <Card className='card'>
        <Suspense fallback={<div><h1>Loading </h1><img style={{height:'100px'}} src='https://res.cloudinary.com/scissorsneedfoodtoo/image/upload/q_auto/v1480497415/chuck-norris-pixel_vqvd2q.png'></img></div>}>   
        <img src={imgSrc}></img>
        <Quote></Quote>
        </Suspense>
    </Card>
  )
}

export default Card