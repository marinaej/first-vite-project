import * as React from 'react';
import { useState, useEffect } from "react";
import axios from "axios";

const API_URL = 'https://api.chucknorris.io/jokes/random/';


function Quote() {

  const [quote, setQuote] = useState();

  useEffect(() => {
    fetchQuote();
  }, []);

  const fetchQuote = async () => {
    try {
      const {data} = await axios.get(`${API_URL}`);
      const fetchedQuote = data.value;
      setQuote(fetchedQuote);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div>
      <div style={{textAlign: 'center', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', fontFamily: 'sans-serif'}}>
        <div>
          <p style={{maxWidth: '300px', wordWrap: 'break-word'}}>{quote}</p>
        </div>
      </div>
    </div>
  );

}

export default Quote
