import React from 'react';
import styled from 'styled-components';
import Card from './components/Card';
import Button from './components/Buttons';

const App = () => {

  const Main = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  padding: 20px;
  `;

  const Container = styled.div`
  display: flex;
  margin: 0 auto;
  max-width: 75%;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  padding: 20px;
  overflow: scroll;
  `;


  return  <Main>
    <Container className='container'>
    <img className="flag" style={{height:'150px'}} src="https://64.media.tumblr.com/c1fdac22926e37351f205794c159a9f6/tumblr_on6e4pAQfm1qhwyc9o1_500.gif" alt="" />
    <div className="container__title">
    <h1>Chuck Norris jokes</h1> 
    <Button></Button>
    </div>
    <img id='flag' className="flag" style={{height:'150px'}} src="https://64.media.tumblr.com/c1fdac22926e37351f205794c159a9f6/tumblr_on6e4pAQfm1qhwyc9o1_500.gif" alt="" />
    </Container>
    <Container className='container__main'>
    <Card></Card>
    <Card></Card>
    <Card></Card>
    <Card></Card>
    <Card></Card>
    <Card></Card>
    <Card></Card>
    <Card></Card>
    <Card></Card>
    <Card></Card>
    </Container>
    </Main>
};

export default App;
